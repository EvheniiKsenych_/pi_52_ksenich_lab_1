﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSaver
{
    class NewKey
    {
        const String AngAlf = "AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz";
        const String SpecialSimbol = "!@#$%^&*()_+-=,./?|';:<>[]{}№";
        const String Kirilica = "АаБбВвГгДдЕеЁёЖжЗзИиЙйКкЛлМмНнОоПпРрСсТтУуФфХчЦцЧчШшЩщЪъЫыЬьЭэЮюЯя" + "ІіЇїЄє";
        const String Number = "0123456789";
        const String AllOut = AngAlf + SpecialSimbol + Kirilica + Number;
        public String GetNewRandomKey()
        {
            String tmp = AllOut;
            Random rnd = new Random();
            String OutPut ="";
            int next, OutSize = 0;
            int SizeMas = AllOut.Length;
            int tmpSize = tmp.Length;
            while (SizeMas != OutSize)
            {
                next = rnd.Next(0, tmpSize);
                OutPut += tmp[next].ToString();
                tmp = ReplaseOne(tmp, next, tmpSize);
                tmpSize--;
                OutSize++; 
            }
            return OutPut;
        }


        String ReplaseOne(String resourse, int index, int size)
        {
            String tmp = "";
            for(int i=0;i< size;i++)
            {
                if (i != index) tmp += resourse[i].ToString();
            }
            return tmp;
        }

        public int GetSourseSuze()
        {
            return AllOut.Length;
        }

        public String GetAllHeh()
        {
            return AllOut;
        }
    }
}
